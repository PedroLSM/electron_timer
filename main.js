const { app, BrowserWindow, ipcMain ,Tray, Menu} = require('electron');
const data = require('./data');
const templateGenerator = require('./template');

let tray = null;
let mainWindow = null;
app.on('ready', () => {
   this.mainWindow = new BrowserWindow({
      title: 'Minha Aplicação',
      width: 600,
      height:400
  });

  tray = new Tray(__dirname + '/app/img/icon-tray.png');
  let template = templateGenerator.geraTrayTemplate(this.mainWindow);
  let trayMenu = Menu.buildFromTemplate(template);
  tray.setContextMenu(trayMenu);

  


  this.mainWindow.loadURL(`file://${__dirname}/app/index.html`);
});

app.on('window-all-closed', () => {
  app.quit();  
});

let sobreWindow = null;

ipcMain.on('abrir-janela-sobre', () => {
      if (sobreWindow == null) {
          sobreWindow = new BrowserWindow({
          title: 'Sobre',
          width: 300,
          height:220,
          alwaysOnTop: true,
          frame: false      
        });
        sobreWindow.on('closed', () => {
        sobreWindow = null;
        });
      }  
    sobreWindow.loadURL(`file://${__dirname}/app/sobre.html`);
});

ipcMain.on('fechar-janela-sobre', () => { 
  sobreWindow.close();

});

ipcMain.on('curso-parado', (event,curso,tempoEstudado) => { 
  data.salvaDados(curso ,tempoEstudado);
});

ipcMain.on('curso-adicionado',(event,novoCurso) => {
  let novoTemplate = templateGenerator.adicionaCursoNoTray(novoCurso,this.mainWindow);
  let novoTrayMenu = Menu.buildFromTemplate(novoTemplate);
  tray.setContextMenu(novoTrayMenu);
  data.salvaDados(novoCurso ,'00:00:00');
})