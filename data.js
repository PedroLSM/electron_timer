const jsonfile = require('jsonfile-promised');
const fs = require('fs');

module.exports = {       
    salvaDados(curso,tempoEstudado){
        let arquivoCurso = __dirname +'/data/' + curso +'.json';
        if (fs.existsSync(arquivoCurso)) {
            //ja existe apenas salva
            this.adicionaTempoAoCurso(arquivoCurso,tempoEstudado);
        }else{
            //criar e salvar
            this.criarArquivoDeCurso(arquivoCurso,{}).then( () => {
                this.adicionaTempoAoCurso(arquivoCurso,tempoEstudado);
            })                
        }
    },
    adicionaTempoAoCurso(arquivoCurso,tempoEstudado){
        let dados = {
            utimoEstudo: new Date().toString(),
            tempoEstudo:tempoEstudado
        }
        jsonfile.writeFile(arquivoCurso,dados,{spaces:2})
                .then( () => {
                    console.log('Tempo Salvo.');
                }).catch((err) => {
                    console.log(err);
                });
    },
    criarArquivoDeCurso(nomeArquivo,conteudoArquivo){
       return jsonfile.writeFile(nomeArquivo,conteudoArquivo)
                .then( () => {
                    console.log('Arquivo Json Criado.');
                }).catch((err) =>{
                    console.log(err);
                });
    },
    pegaDados(curso){
        let arquivoCurso = __dirname +'/data/' + curso +'.json';
        return jsonfile.readFile(arquivoCurso);
                
    },
    buscarTodosCursos(){
       let arquivos =  fs.readdirSync(__dirname + '/data/');
       let cursos = arquivos.map( (arquivo) => {
         return arquivo.substr(0,arquivo.lastIndexOf('.'));
       });

       return cursos;
    }
}